package main // Todos los archivos que tengna el paquete main que es el mismo
// paquete que tiene el fichero main.go va a estar disponible
// desde cualquier otro fichero, por lo cual desde acá se podrá
// acceder a los datos del archivo main.go
type TypePelicula struct {
	Codigo   string `json:"codigo"`
	Titulo   string `json:"titulo"`
	Year     int    `json:"year"`
	Director string `json:"director"`
}

type TypePeliculas []TypePelicula
