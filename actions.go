package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux" // Paquete que permite trabaja con rutas

	"encoding/json"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	coleccion_peliculas = getSession().DB("blog_peliculas").C("peliculas")
	resultados          []TypePelicula
	pelicula_data       TypePelicula
)

/*
var lista_peliculas = TypePeliculas{
	TypePelicula{"1", "Good Bye Lenin!", 2003, "Wolfgang Becker"},
	TypePelicula{"2", "War Horse", 2011, "Steven Spielberg"},
	TypePelicula{"3", "1917", 2019, "Sam Mendes"},
	TypePelicula{"4", "Green Street", 2005, "Lexi Alexander"},
}
*/

func getSession() *mgo.Session {
	db_session, db_err := mgo.Dial("mongodb://localhost:27017")

	if db_err != nil {
		panic(db_err)
	}

	return db_session
}

// Se crean las respectivas secciones con cada método (sección) creado
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Go into func index")
	fmt.Fprintf(w, "Hello world from my web server using GO")
}
func Login(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world from login section")
}
func Register(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world from register section")
}
func Contacto(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello world from contact section")
}
func PeliculasGet(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintf(w, "Movies list")
	err := coleccion_peliculas.Find(nil).Sort("-_id").All(&resultados)

	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Println("Resultados: ", resultados)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)

	json.NewEncoder(w).Encode(resultados)
}
func PeliculaGet(w http.ResponseWriter, r *http.Request) {
	parametros := mux.Vars(r)         // Ni la mas puta idea, buscar info
	p_pelicula_id := parametros["id"] // Creo que se asignan los parametros en una variable.

	//fmt.Fprintf(w, "Pelicula: %s", pelicula_id) // Cuando se envía un parametro, se especifica el valor con %s, y en un nuevo argumento "pelicula_id" se pasa el valor que tomará
	comprueba_id := bson.IsObjectIdHex(p_pelicula_id)
	pelicula_id := bson.ObjectIdHex(p_pelicula_id)
	resultado := TypePelicula{}

	coleccion_peliculas.FindId(pelicula_id).One(&resultado)

	if comprueba_id == true {
		fmt.Println("true", pelicula_id)
	} else {
		fmt.Println("false")
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(resultado)
}
func PeliculaPost(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var nueva_pelicula TypePelicula

	err := decoder.Decode(&nueva_pelicula)

	if err != nil {
		panic(err)
	}

	defer r.Body.Close()

	//session := getSession()
	//session.DB("blog_peliculas").C("peliculas").Insert(nueva_pelicula)
	err = coleccion_peliculas.Insert(nueva_pelicula)

	if err != nil {
		w.WriteHeader(500)
		return
	}

	//lista_peliculas = append(lista_peliculas, nueva_pelicula)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(nueva_pelicula)
}
func PeliculaPut(w http.ResponseWriter, r *http.Request) {
	parametros := mux.Vars(r)         // Ni la mas puta idea, buscar info
	p_pelicula_id := parametros["id"] // Creo que se asignan los parametros en una variable.

	//fmt.Fprintf(w, "Pelicula: %s", pelicula_id) // Cuando se envía un parametro, se especifica el valor con %s, y en un nuevo argumento "pelicula_id" se pasa el valor que tomará
	comprueba_id := bson.IsObjectIdHex(p_pelicula_id)

	if comprueba_id == false {
		w.WriteHeader(404)
		fmt.Println("entra comprueba_id")
		return
	}

	pelicula_id := bson.ObjectIdHex(p_pelicula_id) // Devuelve id como objeto json y no como texto plano

	decoder := json.NewDecoder(r.Body) // Decodifica el json

	var movie_data TypePelicula

	decoder.Decode(&movie_data)

	defer r.Body.Close()

	documento := bson.M{"_id": pelicula_id}
	change := bson.M{"$set": movie_data}

	coleccion_peliculas.Update(documento, change)

	/*
		if err != nil {
			panic(err)
			w.WriteHeader(404)
			return
		}
	*/

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(movie_data)
}
func PeliculaDelete(w http.ResponseWriter, r *http.Request) {
	parametros := mux.Vars(r)         // Ni la mas puta idea, buscar info
	p_pelicula_id := parametros["id"] // Creo que se asignan los parametros en una variable.

	//fmt.Fprintf(w, "Pelicula: %s", pelicula_id) // Cuando se envía un parametro, se especifica el valor con %s, y en un nuevo argumento "pelicula_id" se pasa el valor que tomará
	//comprueba_id := bson.IsObjectIdHex(p_pelicula_id)
	//pelicula_id := bson.ObjectIdHex(p_pelicula_id)
	//resultado := TypePelicula{}

	coleccion_peliculas.RemoveId(bson.ObjectIdHex(p_pelicula_id))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode("Correcto")
}
