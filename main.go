package main

import (
	"log"
	"net/http"
	// Paquete que permite trabaja con rutas
)

func main() {
	// Se crea una instancia router como enrutador utilizando el paquete mux
	//router := mux.NewRouter().StrictSlash(true)
	router := NewRouter()
	// Se definen las rutas (requests handle) utilizando el enrutador "router" creado
	/*
		router.HandleFunc("/", Index)
		router.HandleFunc("/login", Login)
		router.HandleFunc("/register", Register)
		router.HandleFunc("/contacto", Contacto)
		router.HandleFunc("/peliculas", MuestraPeliculas)
		router.HandleFunc("/pelicula/{id}", MuestraPelicula) // Se envia el parametro id, creado con llaves
	*/
	/*
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "Hello, World from my web server using Go")
		})
	*/

	//server := http.ListenAndServe(":8000", nil)
	/******************************************************************************************
		El parametro nil corresponde al enrutador principal del servidor HTTP, por defecto
		es nulo, lo que significa user el enrutador predeterminado del paquete net/http. Para
		utilizar un nuevo enrutador (creado con mux) se debe reemplazar el valor nil con
		la variable del enrutador creado "router" como se muestra debajo
	******************************************************************************************/
	server := http.ListenAndServe(":8000", router)
	log.Fatal(server)
}
